# flask-tdd-docker

Deployed on Heroku:

https://lit-lowlands-95420.herokuapp.com/users

# Test-Driven Development with Python, Flask, and Docker

[![pipeline status](https://gitlab.com/johnfkraus/flask-tdd-docker/badges/master/pipeline.svg)](https://gitlab.com/johnfkraus/flask-tdd-docker/commits/master)


$ docker-compose down -v
$ docker-compose up -d --build

Initialize the database:

$ docker-compose exec api python manage.py recreate_db
$ docker-compose exec api python manage.py seed_db

docker-compose exec api python -m pytest "src/tests" -p no:warnings
$ docker-compose exec api flake8 src
$ docker-compose exec api black src
$ docker-compose exec api isort src

Browse:

Get all users:

localhost:5004/users

## Test on Heroku:

Enter the shell on Heroku:

heroku run bash --app lit-lowlands-95420

Inside the shell enter the Flask shell (Chapter 15):

export FLASK_APP=src/__init__.py
flask shell
app.config["SECRET_KEY"]




